# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

admin = User.create([
  {
    email: 'info@bssc.vn',
    password: '123456',
    password_confirmation: '123456',
  }
])

puts "Admin created: #{admin.inspect}"

setting = Setting.create([
  {
    name: "question_step",
    value: "0"
  }
])

puts "Setting created: #{setting.inspect}"
