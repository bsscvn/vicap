class CreateQuestions < ActiveRecord::Migration[7.0]
  def change
    create_table :questions do |t|
      t.string :word, null: false, default: ""
      t.text :related_words, array: true, default: []
      t.integer :status, default: 0

      t.timestamps
    end

    add_index :questions, :related_words, using: 'gin'
  end
end
