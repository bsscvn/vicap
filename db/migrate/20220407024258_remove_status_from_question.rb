class RemoveStatusFromQuestion < ActiveRecord::Migration[7.0]
  def change
    remove_column :questions, :status
  end
end
