class AddStatusToSetting < ActiveRecord::Migration[7.0]
  def change
    add_column :settings, :status, :integer, default: 0
  end
end
