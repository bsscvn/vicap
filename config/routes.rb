Rails.application.routes.draw do
  devise_for :users

  root "game#index"
  get "/end", to: "game#end"
  get "/rank", to: "game#rank"

  resources :questions, only: [:show] do
    post "/close", to: "questions#close"
  end
  
  resources :user, only: [:show] do
    collection do
      post "/answer", to: "user#answer"
    end
  end
end
