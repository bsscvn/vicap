class Setting < ApplicationRecord
  enum :status, [ :open, :closed ], suffix: true, default: :open

  after_update_commit -> {
    broadcast_replace_to "setting_#{self.id}", partial: "user/form", locals: {question: self}, target: "setting_#{self.id}"
  }
end
