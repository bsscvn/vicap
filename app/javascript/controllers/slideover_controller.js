import { Controller } from "@hotwired/stimulus"
import { useTransition } from "stimulus-use"

export default class extends Controller {
  static targets = ["overlay", "panel"]

  connect() {
    useTransition(this, {
      element: this.panelTarget,
      enterActive: 'transform transition ease-in-out duration-500 sm:duration-700',
      enterFrom: 'translate-x-full',
      enterTo: 'translate-x-0',
      leaveActive: 'transform transition ease-in-out duration-500 sm:duration-700',
      leaveFrom: 'translate-x-0',
      leaveTo: 'translate-x-full',
      transitioned: false,
    });
  }

  close() {
    setTimeout(() => this.overlayTarget.classList.add("hidden"), 400);
    this.leave();
  }

  open() {
    this.overlayTarget.classList.remove("hidden");
    this.enter();
  }

  toggle() {
    this.toggleTransition();
  }
}
