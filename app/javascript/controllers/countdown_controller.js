import { Controller } from "@hotwired/stimulus"
import moment from "moment";

export default class extends Controller {
  static values = { endtime: String, next: String }

  connect() {
    const end = moment.utc(this.endtimeValue);
    const url = window.location.href + "/close";

    const countdown = setInterval(() => {
      let now = moment.utc();
      let distance = end - now;

      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      this.element.innerHTML = seconds;

      if (distance < 0) {
        clearInterval(countdown);
        this.element.innerHTML = "";

        const question = document.getElementById("question");
        question.classList.add("scale-50");

        const results = document.getElementById("results");
        results.classList.remove("hidden");
        results.classList.add("block");

        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          },
        }).then(res => res.json())
          .then(res => console.log(res));

        setTimeout(() => {
          window.location.href = this.nextValue;
        }, 10000);
      }
    }, 1000);
  }
}
