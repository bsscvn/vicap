class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :close]
  skip_before_action :verify_authenticity_token, only: [:close]

  def show
    if current_user.email != "info@bssc.vn"
      redirect_to user_path(current_user.id)
    end

    question_step = Setting.first
    question_step.update(value: @question.id, status: "open")
  
    @answers = @question.answers.order(created_at: :desc)

    if Question.where("id > ?", @question.id).first.present?
      @next_question = question_path(Question.where("id > ?", @question.id).first)
    else
      @next_question = end_path
    end
  end

  def close
    question_step = Setting.first
    question_step.update(status: "closed")
  end

  private

  def set_question
    @question = Question.find(params[:id] || params[:question_id])
  end
end
