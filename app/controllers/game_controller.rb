class GameController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.email != "info@bssc.vn"
      redirect_to user_path(current_user.id)
    end

    question_step = Setting.first
    question_step.update(value: 0)

    qrcode = RQRCode::QRCode.new(new_user_session_url)
    @qr_svg = qrcode.as_svg(fill: "000", color: "FFF", shape_rendering: "crispEdges", module_size: 10, standalone: true, use_path: true)
  end

  def end
  end

  def rank
    @users = User.where.not(email: "info@bssc.vn").sort_by(&:answer_score_sum).reverse!
  end
end
