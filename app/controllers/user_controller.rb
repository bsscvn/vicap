class UserController < ApplicationController
  layout "user"

  before_action :authenticate_user!

  def show
    @question = Setting.first
  end

  def answer
    if current_user.answers.find_by_content(params[:content]).blank?
      question = Question.find(params[:question_id])
      if question.related_words.any?{ |s| s.gsub(/[^0-9A-Za-z]/, "").delete(" ").singularize.casecmp( params[:content].gsub(/[^0-9A-Za-z]/, "").delete(" ").singularize ) === 0 }
        score = 1
      else
        score = 0
      end

      answer = current_user.answers.build(content: params[:content], question_id: params[:question_id], score: score)
      if answer.save
        redirect_to user_path(current_user), notice: "Keyword was successfully submitted"
      else
        redirect_to user_path(current_user), alert: "Error: keyword was not submitted"
      end
    else
      redirect_to user_path(current_user), alert: "You have already submitted this keyword"
    end
  end
end
