require "csv"

namespace :import do
  task users: :environment do
    csv_text = File.read("lib/tasks/users.csv")
    csv = CSV.parse(csv_text, quote_char: '"', col_sep: ",", row_sep: :auto, headers: true)
    csv.each do |row|
      user = User.new(
        username: row["username"],
        first_name: row["first_name"],
        email: row["email"],
        password: "123456",
        password_confirmation: "123456",
      )

      if user.save
        puts "User created: #{user.inspect}"
      else
        puts "User not created: #{user.inspect}"
      end
    end
  end

  task questions: :environment do
    csv_text = File.read("lib/tasks/questions.csv")
    csv = CSV.parse(csv_text, quote_char: '"', col_sep: ",", row_sep: :auto, headers: true)
    csv.each do |row|
      question = Question.new(
        word: row["Name"],
        related_words: row["Keyword"].split(",")
      )

      if question.save
        puts "Question created: #{question.inspect}"
      else
        puts "Question not created: #{question.inspect}"
      end
    end
  end
end
